package codemantra.ADH.pages;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import codemantra.ADH.TestBase.BaseClass;

public class ProcessElementViewerPage extends BaseClass {

	@FindBy(xpath = "//i[@class='fa fa-minus-circle']")
	WebElement clearorder;

	@FindBy(xpath = "//i[@class='glyphicon glyphicon-question-sign']")
	WebElement help;

	@FindBy(xpath = "//h3[contains(text(),'Help - Shortcuts')]")
	WebElement helpmodalbox;

	@FindBy(xpath = "//div[@id='showHelp']//h3")
	WebElement helpshortcut;

	@FindBy(xpath = "//div[@class='modal-body']/p")
	WebElement clearreadingorder;

	@FindBy(xpath = "//button[@id='modal-btn-si']")
	WebElement yes;

	@FindBy(xpath = "//div[@id='closeMeta']")
	WebElement metaclose;

	@FindBy(xpath = "//div[@class='closepop'][@id='closeHelp']")
	WebElement helpclose;

	@FindBy(xpath = "//div[@id='closeTgList']")
	WebElement taglistclose;

	@FindBy(xpath = "//div[@class='rdorder']")
	WebElement reorder;

	@FindBy(xpath = "//div[@id='metaDetails']")
	WebElement metabox;

	@FindBy(xpath = "//div[@id='TagList']")
	WebElement tagList;

	@FindBy(xpath = "//div[@id='inspct']")
	WebElement inspectmodalbox;

	@FindBy(xpath = "//div[@id='closeinspct']")
	WebElement inspectclose;

	// @FindBy(xpath = "//button[contains(text(),'Cancel')]")
	// WebElement approvecancel;

	// @FindBy(xpath = "//div[@class='modal-content']")
	// @FindBy(xpath = "//div[@class='modal-content']/div[2]/p")
	// @FindBy(xpath = "//p[@class='ng-binding']")
	// @FindBy(xpath = "//div[@class='modal-content']//div[2]/p")
	// WebElement modalcontent;

	// @FindBy(xpath = "//button[@id='modal-btn-si']")
	// WebElement ignore_yes;

	public ProcessElementViewerPage() {
		PageFactory.initElements(driver, this);
	}

	public void shortCutKeys() throws InterruptedException {
		Actions action = new Actions(driver);

		action.moveToElement(clearorder).perform();
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", clearorder);
		// if((clearreadingorder != null) && (clearreadingorder.isDisplayed()))
		// {
		Thread.sleep(1000);

		action.moveToElement(yes).build().perform();
		yes.click();
		// }
		if (!reorder.isDisplayed()) {
			Assert.assertTrue(true);
		}
		Thread.sleep(2000);

		// help button
		action.moveToElement(help).perform();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", help);
		Thread.sleep(2000);

		action.dragAndDropBy(helpmodalbox, 900, 10).build().perform();
		String expected_h = helpshortcut.getText();
		String actual_h = "Help - Shortcuts";
		Assert.assertEquals("Validation of shortcut keys", expected_h, actual_h);
		Thread.sleep(2000);

		// reorder number in zones

		action.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("A").keyUp(Keys.CONTROL).keyUp(Keys.SHIFT).build()
				.perform();
		System.out.println("Zones are reordered by pressing Shortcut key CTRL+SHIFT+A");

		if (reorder.isDisplayed()) {
			Assert.assertTrue(true);
		}

		Thread.sleep(2000);
		// Save button shortcut key

		action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("S").build().perform();
		action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();

		driver.manage().timeouts().pageLoadTimeout(1000, TimeUnit.SECONDS);
		System.out.println("Save is performed by pressing keys CTRL+Alt+ S");
		Thread.sleep(5000);

		// metadata shortcut key

		action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("M").build().perform();
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(ExpectedConditions.visibilityOf(metabox));

		System.out.println("Metadata modal box is displayed by pressing shortcut key CTRL+Alt+M");

		action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", metaclose);
		Thread.sleep(2000);

		// taglist shortcut key

		action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("T").build().perform();
		wait.until(ExpectedConditions.visibilityOf(tagList));
		System.out.println("Tag List modal box is displayed by pressing keys CTRL+Alt+T");
		action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();
		action.dragAndDropBy(tagList, 60, 80).build().perform();
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", taglistclose);

		Thread.sleep(2000);

		// inspect shortcut key

		action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("I").build().perform();
		wait.until(ExpectedConditions.visibilityOf(inspectmodalbox));
		System.out.println("Tag List modal box is displayed by pressing keys CTRL+Alt+I");
		action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();

		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", inspectclose);

		Thread.sleep(2000);

		// Zoom In Zoom Out shortcut key
		// zoom out
		for (int z = 1; z <= 2; z++) {
			action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("-").pause(Duration.ofSeconds(2)).build().perform();
			action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();
		}
		System.out.println("Zoom Out is performed by pressing keys CTRL+ Alt+ -");

		Thread.sleep(2000);
		// zoom in
		for (int z = 1; z <= 2; z++) {
			action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("+").pause(Duration.ofSeconds(2)).build().perform();
			action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();
		}
		System.out.println("Zoom In is performed by pressing keys CTRL+Alt+ +");

		Thread.sleep(2000);

		// Approve Button

		// action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("A").build().perform();
		// if ((modalcontent != null) && (modalcontent.isDisplayed()))
		// if (modalcontent.isDisplayed())

		// {

		// action.moveToElement(ignore_yes).build().perform();
		// Thread.sleep(500);
		// action.keyUp(Keys.CONTROL).keyUp(Keys.ALT).build().perform();
		// ignore_yes.sendKeys(" ");
		// ignore_yes.click();
		// }

		// wait.until(ExpectedConditions.elementToBeClickable(approvecancel));
		// Thread.sleep(2000);
		// approvecancel.click();

		// System.out.println("Approve shortcut key is performed by pressing keys
		// CTRL+Alt+ A");
		// Thread.sleep(2000);
		// driver.navigate().refresh();
		// driver.manage().timeouts().pageLoadTimeout(200, TimeUnit.SECONDS);
		// shortcut keys for move box up , down,left ,right

		try {
			List<WebElement> l = driver.findElements(By.xpath("//div[@class='elementx']"));

			int no_of_elements = l.size();
			System.out.println("Number of elements found on current page is  " + no_of_elements);
			for (int e = 0; e < no_of_elements; e++) {
				String element = l.get(e).getText();
				if (e == 0) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", l.get(e));
					// shortcut key to move box left
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.ALT)
								.sendKeys(l.get(e), Keys.ARROW_LEFT).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("Zoned element box is moved left by pressing keys CTRL+Alt+ Left Arrow");
					Thread.sleep(1000);
					// shortcut key to move box right
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.ALT)
								.sendKeys(l.get(e), Keys.ARROW_RIGHT).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("Zoned element box is moved right by pressing keys CTRL+Alt+ Right Arrow");
					Thread.sleep(1000);
					// shortcut key to move box up
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.ALT)
								.sendKeys(l.get(e), Keys.ARROW_UP).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("Zoned element box is moved up by pressing keys CTRL+Alt+ Up Arrow");
					Thread.sleep(1000);
					// shortcut key to move box down
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.ALT)
								.sendKeys(l.get(e), Keys.ARROW_DOWN).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("Zoned element box is moved right by pressing keys CTRL+Alt+ DOWN Arrow");
					Thread.sleep(1000);

					// shortcut key to shrink right side of box
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.SHIFT)
								.sendKeys(l.get(e), Keys.ARROW_LEFT).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("shortcut key (CTRL+SHIFT+ Left Arrow)to shrink right side of box");
					Thread.sleep(1000);

					// shortcut key to expand right side of box
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.SHIFT)
								.sendKeys(l.get(e), Keys.ARROW_RIGHT).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("shortcut key (CTRL+SHIFT+ Right Arrow)to expand right side of box");
					Thread.sleep(1000);
					// shortcut key to shrink bottom side of box
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.SHIFT)
								.sendKeys(l.get(e), Keys.ARROW_UP).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("shortcut key (CTRL+SHIFT+ UP Arrow)to shrink bottom side of box");
					Thread.sleep(1000);

					// shortcut key to expand bottom side of box
					for (int p = 0; p <= 5; p++) {

						action.keyDown(l.get(e), Keys.CONTROL).keyDown(l.get(e), Keys.SHIFT)
								.sendKeys(l.get(e), Keys.ARROW_DOWN).pause(Duration.ofSeconds(2)).build().perform();

					}
					System.out.println("shortcut key (CTRL+SHIFT+ Down Arrow)to expand bottom side of box");
					Thread.sleep(1000);

					if (element.equalsIgnoreCase("Figure") || (element.equalsIgnoreCase("Table"))
							|| (element.equalsIgnoreCase("Formula"))) {

						((JavascriptExecutor) driver).executeScript("arguments[0].click();", l.get(e));
						Thread.sleep(1000);
						System.out.println("The selected element to remove zone is  : " + element);
						action.sendKeys(Keys.DELETE).build().perform();
						// action.keyUp(Keys.DELETE).build().perform();
						Alert alert = driver.switchTo().alert();

						System.out.println("The alert message is :" + alert.getText());
						Thread.sleep(1000);
						alert.accept();
						if (l.get(e).isDisplayed()) {
							System.out.println("Selected zone for " + element + " is not removed");
						}

						// Assert.assertNotNull("zone is not deleted", l.get(e));

					}
					if (element.equalsIgnoreCase("P")) {
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", l.get(e));
						action.sendKeys(Keys.DELETE).build().perform();

						System.out.println("Selected zone for " + element + " is  removed");

					}
				}

				break;
			}
		} catch (NoSuchElementException e) {
			System.out.println("Unable to locate element");
		} catch (UnhandledAlertException e) {
			System.out.println("unexpected alert open");

		}
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", helpclose);
		
	}

}
